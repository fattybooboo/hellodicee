//
//  ViewController.swift
//  HelloDicee
//
//  Created by 劉家維 on 2018/7/30.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var diceA = dice()
    let diceImageArray : [String] = ["dice1", "dice2", "dice3", "dice4", "dice5", "dice6"]
    
    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    @IBOutlet weak var diceImageView3: UIImageView!
    @IBOutlet weak var diceImageView4: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       updateToDiceNumber()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rollButtonPressed(_ sender: UIButton) {
        
        updateToDiceNumber()
        
    }
    
    func updateToDiceNumber() {
        
        diceA.rDiceIndex1 = Int(arc4random_uniform(6))
        diceA.rDiceIndex2 = Int(arc4random_uniform(6))
        diceA.rDiceIndex3 = Int(arc4random_uniform(6))
        diceA.rDiceIndex4 = Int(arc4random_uniform(6))
        
        diceImageView1.image = UIImage(named: diceImageArray[diceA.rDiceIndex1])
        diceImageView2.image = UIImage(named: diceImageArray[diceA.rDiceIndex2])
        diceImageView3.image = UIImage(named: diceImageArray[diceA.rDiceIndex3])
        diceImageView4.image = UIImage(named: diceImageArray[diceA.rDiceIndex4])
        
        
    }
    
    //Prepare Shake.
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        
        updateToDiceNumber()
        
    }
    
    
    
    
}

